# Module Federation

## How to...

### Step 1

Add module federation to all projects (shell & mfe) using `@angular-architects` tool

### Step 2

Make d.ts file with modules that will be lazy loaded:

`declare module 'mfe1/ModuleName`

## Resources

- https://www.youtube.com/watch?v=oXGeywCIQDU
- https://www.angulararchitects.io/aktuelles/the-microfrontend-revolution-module-federation-in-webpack-5/ : Awesome series of articles by Manfred about Webpack 5 Module Federation and how to use it with Nx workspace and Angular - **awesome stuff**
  - https://www.angulararchitects.io/aktuelles/multi-framework-and-version-micro-frontends-with-module-federation-the-good-the-bad-the-ugly/ - how to make it work with Web Components and different framework versions (Using Module Federation together with Web Components/ Angular Elements). Important: **shows how to reuse ngZone!**.
- https://github.com/angular-architects/module-federation-plugin - module federation plugin for webpack: seamlessly using Webpack Module Federation with the Angular CLI.
- https://module-federation.github.io/ - official page and book
- https://medium.com/agorapulse-stories/building-a-micro-frontends-architecture-in-2021-with-angular-and-webpack-module-federation-50d073617645 : not much value, just overview
