- stack vs heap: stack is managed by operating system, heap by 
- GC is a program that runs and clears the memory
- memory is allocated on heap
- V8 allocates objects into six spaces
  - new space: smaller, newer, fresh objects
  - old space
    - old pointer space: if object is actively used, it moves to this space
    - old data space: objects that has objects with no references to other objects
  - large object space
  - code space
  - map space
- if memory is not reachable, then it can be garbage collected. If you traverse via heap and stuff is not reachable, then it can be GC
- how GC happens:
  - GC stops the world, stops the program and runs short GC cycles
    - in new space this is called "Scavenger(Minor GC)": stuff it traversed and copied if reached. Allocation in “new space” is very cheap: there is an allocation pointer which we increment whenever we want to reserve space for a new object. When the allocation pointer reaches the end of the new space, a minor GC is triggered. This process is also called Scavenger and it implements Cheney’s algorithm. It occurs frequently and uses parallel helper threads and is very fast.
    - in old space this is called mark & sweep collector - it marks live objects in first run, and frees non marked objects in second run: 
- V8 manages the heap memory by garbage collection. In simple terms, it frees the memory used by orphan objects, i.e, objects that are no longer referenced from the Stack directly or indirectly (via a reference in another object) to make space for new object creation.


- check mark and sweep 
- check devtools and memory dumps to compare memory snapshots
- there's a `npm i heapdump` tool for node
- check nodejs flags to troubleshoot memory issues (e.g. it will tell you when GC happens): --trace-gc
- check what are the javascript engines
  - V8

## What can leak

- Retaining tree: if any of the object's properties is used, the whole object must be kept in memory
- Leaking DOM nodes / detached DOM nodes: if DOM node is removed from DOM but we keep reference to in the code, it will still be kept in memory

### Tips

- don't use anonymous functions for event callbacks
- pass portions of the objects that are only needed (why? recall retaining tree)
- to find memory leaks use devtools heap allocations and heap snapshots

-----

Ivy and incremental DOM:

- Every component gets compiled into a series of instructions. These instructions create DOM trees and update them in-place when the data changes.
- no separate ngFactory files, all decorator metadata is shipped with the component class as static fields
- In IVY every component is its own factories directly
- IVY is better treeshakable, composed of smaller functions
  - e.g. ViewEngine interpreted ngFactories at runtime, had to support all framework features and was impossible to treeshake
- incrementality: compilation is incremental. Libraries are in npm already AOT compiled and safe to use.
- ng build only builds app

-----

- angular.js 
  - angular.js had dirty checking, 1 digest cycle only (2 in dev mode)
  - dirty checking code was very generic, not treeshakable . VM don't like this code, cannot optimize it
- angular:
  - use one way data flow, no dirty checking, number of digest cycles was more than 1 (limited)
  - compiler generates code that VM like and can optimize
  - Angular is very fast doing change detection for every single component as it can perform thousands of checks during milliseconds using inline-caching which produces VM-optimized code.
  - Default: This default strategy checks every component in the component tree from top to bottom every time an event triggers change detection (like user event, timer, XHR, promise and so on).
  - OnPush (simple, predictable): Using this strategy, Angular knows that the component only needs to be updated if:
    - the input reference has changed
    - the component or one of its children triggers an event handler
    - change detection is triggered manually
    - an observable linked to the template via the async pipe emits a new value


## Resources

- https://deepu.tech/memory-management-in-v8/
- https://www.youtube.com/watch?v=CRoR_0K586I
- https://www.youtube.com/watch?v=RJRbZdtKmxU
- https://www.youtube.com/watch?v=lXj6j9hVGLQ : best mark sweep collector explanation
