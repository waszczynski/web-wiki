# Accessibility

## Accessible name

What is accessible name: https://knowledge.evinced.com/concepts/accessible-names

The purpose of the accessible name is to differentiate between UI elements, give them context, and allow the user to access them directly. Some elements require an accessible name for assistive technologies to read them correctly and to meet the accessibility requirements.

## Roles

Role attribute is meant to apply semantics to non-semantic elements (e.g. <div>, <span>) or to extend the semantics of an element, for example like applying role=”switch” to a checkbox element.

- Don't set `outline: 0` for buttons and other focusable elements if you don't hadle it
- Follow HTML semantics, e.g. `a` tag should have `href` attribute, otherwise they are not focusable

### Examples

- `role="region"` : Is used in combination with `aria-label` which is used to provide meaningful name to the element. Enables easy navigation via landmark regions short key when using screen readers
- `role= “listbox”` : Is used in combination with aria-activedescendant which holds ID of the current focused option and aria-multiselectable to indicate if more than one options can be selected Indicates that the element contains a list of options from which user may select one or more options.
  - if e.g. `<ul>` element has a role value of `“listbox”` for screen readers to read it as a select-list (`<select>`), the `listbox` role requires that its direct children will have an `“option”` role so screen readers can parse and read it correctly.
- `role= “option”` : Used in combination with `aria-selected` which indicates whether option is selected or not Element(s) with option role is/are owned by element with role of listbox indicating that is an option that user may be able to select.
- `role= “alert”` : Used in combination with `aria-live`. Screen readers or assistive devices rely on these elements to relay state changes or announces any messages thrown to user (E.g., Validation, error or informational messages shown to the user)
- `aria-describedby=“=#ID of an element with more information”` : Holds list of ID’s of an element which describes object in context

## Tools

- NVDA (firefox windows) or JAWS (windows edge) or VoiceOver (mac)
- axe chrome extension
- Accessibility Insights chrome extension
- WAVE Evaluation Tool
- Lighthouse
- Chrome Dev tools:
  - Landmark Navigation via Keyboard or Pop-up
  - Accessibility inspector
  - Color picker (has contrast ratio info)

### Voice Over

- To enable it, use `fn + cmd + f5`
- To stop reading it, use `control`

## Focus management

- move the user's focus as as a part of interaction (e.g. modal dialog is show)
- make element reachable and operable
- allow interaction with tab / esc / arrow keys
- check focus CSS styles
- lock focus i.e. when modal dialog opens, don't allow to move focus outside of modal dialog
- by default, many elements are focusable by default (`button`, `a`, etc)

> It is easy to write tests for focus management. Just raise clicks / keyboard interactions and assert if proper elements gain focus.

> For integration / page level tests, use Cypress etc

### Keyboard keys

![Typical shortcuts](keyboard-a11y.png)

or

![Combo box interaction](keyboard-a11y-2.png)

### `tabindex`

- `tabindex=0` : element is focusable. This allows elements that are not natively focusable (such as `<div>, <span>, <p>`, and `<a>` with no href) to receive keyboard focus.
- `tabindex=-1` : element is not focusable with keyboard.` tabindex="-1"` allows that element to receive programmatic focus. This means focus can be set to it using `focus()` scripting. This may be useful for elements that should not be navigated to directly using the Tab key, but need to have keyboard focus set to them.
- any other positive value makes element focusable, the biggest values get focus first

> Awesome explanation: https://webaim.org/techniques/keyboard/tabindex

Use `tabindex` ONLY for **interactive elements**. Screen readers are already able to navigate between headings and other text elements.

> If you add `tabindex` to make element focusable (e.g. `div`), you'll probably need to add it a `role` and `aria-label` as well. If you need to make if fully interactive, then `click` and `keydown` is also necessary

### Outline

Do not use this style:

```css
*:focus {
  outline: none;
}
```

Start with e.g.

```css
*:focus,
*:hover {
  outline: 5px auto blue;
}
```

#### Outline on mouse vs keyboard focus

Sometimes showing focus after mouse interaction is undesirable.

`:focus-visible` is new pseudoclass (has polyfill for older browser) that can be used to style outline only on keyboard based focus.

Also `Angular CDK` directives provides this feature.

### Skip links

This is a technique where hidden elements (usually with `opacity: 0`) are become visible only when they gain focus via keyboard, and "clicking" on them, move the focus to completely different area (aka skip link target), e.g. skip link from main content to navigation, or skip link from navigation to main content.

It also refers to technique where after navigation a focus is put into main content skip link.

Nice example is here: https://www.w3.org/TR/wai-aria-practices/examples/landmarks/HTML5.html#

## Announcements

The goal is to notify user without moving the focus.

This is achieved with (they do the same thing):

- `role={status|alert}`
- `aria-live={polite|assertive}`

Just put text in elements with this attributes and it will be read:

```html
<div aria-live="polite" role="status">No results found.</div>
```

## Events

- `focusin` & `focusout` (they bubble) and `focus` & `blur` (not bubble)

```js
const form = document.getElementById("form");

form.addEventListener("focusin", (event) => {});

form.addEventListener("focusout", (event) => {});
```

- `document.activeElement` is always element with focus

- `click` and `keydown` : usually to make element interactive, click is enough if you're using semantically appropriate elements (like `button`). If you're using elements that do not have `role` and semantic meaning, and make them behave like others e.g. `div` with `role=button`, `keydown` is also required.

- in order to make element fully ignored, set `aria-hidden=true` and `tabindex=-1` or use `inert` polyfill. Use it with e.g. when modal dialog is show and nothing else should be accessible except elements inside modal.

## Reduced animations

Use media query `prefers-reduced-motion: reduce` to disable animations.

## Accessible forms

- use `for` attribute on labels that point to id of form element:

```html
<div class="form-group">
  <label for="exampleInputEmail1">Email address</label>
  <input
    type="email"
    class="form-control"
    id="exampleInputEmail1"
    placeholder="Email"
  />
</div>
```

## Semantic HTML

- use headings and landmarks
  - screen readers allow to navigate through `h` tags
  - HTML5 elements such as `main`, `nav`, and `aside` act as landmarks. You can also use tools like Microsoft's Accessibility Insights extension to visualize your page structure and catch sections that aren't contained in landmarks
- don't use `div` etc. unless needed. Try to find a semantically matching HTML element first (`section`, `nav` etc.)

### Landmarks

Landmark regions also enable screen reader users to quickly jump to that section instead of tabbing throughout the page:

![Typical landmarks](landmarks-a11y.png)

## CSS Tools

- `sr-only` aka `visually-hidden` CSS classes
- `display: none`, `visibility: hidden`, `opacity: 0`

> If using Angular CDK and a11y module, there's already a `cdk-visually-hidden` class

## Component guidelines

- keyboard usage for dropdown element: use tab to get to component, enter to "open component" (expand it and put focus on first dropdown element), arrow keys to navigate through component, and escape to get where I came from (collapsed element)
- are you creating a custom component? Check out best authoring practices: https://w3c.github.io/aria-practices/ .

## Other aria attributes

- `aria-haspopup="true"` : if element, like button, triggers display of other element (like dropdown menu), then use it
- `aria-controls="id"` : same use-case as `aria-haspopup`, `id` points to `id` of triggered element
- `role='list'` : use it if element contains a list and you would like to make screen reader announce how many items it has

## Testing a11y

- linting : is ok, but it does not know about CSS nor JavaScript effects
- component level testing : e.g. `@testing-library/angular` promotes writing tests with accessibility API

> Use tools like `axe-core`, `cypress-axe`, `jest-axe`, `accessibilityjs` to detect static issues. These tools catch up to 50% of issues / rules.

## Angular CDK a11y module

This is just awesome module...

### Key managers

- `ActiveDescendantKeyManager` and `FocusKeyManager` : use it to implement managing of the active option in a list of items based on keyboard interaction. Intended to be used with components that correspond to a `role="menu"` or `role="listbox"` pattern.

Key manager works with items (options) and options must implement some interfaces in order to have full accessibility support.

- `ActiveDescendantKeyManager` is intended to be used with `aria-activedescendant` attribute. In that case, each list item should implement `Highlightable` interface.
- `FocusKeyManager` is intended to be used when options will receive the browser focus directly. In that case, each list item has to implement the `FocusableOption` interface

https://indepth.dev/posts/1147/doing-a11y-easily-with-angular-cdk-keyboard-navigable-lists article explains it all very easily and provides step by step algorithm for components with key managers.

### Focus trap

- `cdkFocusTrap` Prevent focus from leaving particular element, useful for modals.
- `cdkFocusRegionStart`, `cdkFocusRegionEnd` and `cdkFocusInitial` : use to explicitly define trapped area

### FocusMonitor

Wanna know how and when elements were focused ? Use `FocusMonitor` to start monitoring elements by calling `monitor` and making a subscription. When element is monitored, it will be decorated with additional CSS classes that tell how element was focused (provides easy way to style elements based on how element gained focus!).

`cdkMonitorElementFocus` and `cdkMonitorSubtreeFocus` : directives that do the same as `FocusMonitor`. Both have `cdkFocusChange` that will emit event when focus is changed.

## LiveAnnouncer

Simple as that (you don't need explicit `aria-live` etc):

```ts
@Component({...})
export class MyComponent {

 constructor(liveAnnouncer: LiveAnnouncer) {
   liveAnnouncer.announce("Hey Google");
 }
}
```

### InteractivityChecker

Wanna know what are the 'properties' of the elements in terms of interactivity (if visible, tabbable, focusable etc) ? Use this service.

## Resources

- https://moduscreate.com/blog/adding-keyboard-navigation-to-angular-lists-using-angular-cdk-listkeymanager/ : example of `ListKeyManager`
- https://netbasal.com/accessibility-made-easy-with-angular-cdk-1caaf3d98de2 : nice example of angular CDK usage and key managers, however not fully accessible (it only shows how to do keyboard navigation) with `ActiveDescendantKeyManager`
- https://keycode.info/ - how to get key code quickly
- https://www.evinced.com/blog/creating-accessible-combo-boxes/ - awesome combobox with listbox example, step by step with keyboard interaction guidelines
- https://frontendmasters.com/courses/javascript-accessibility/
  - https://marcysutton.github.io/js-a11y-workshop/slides/
  - https://github.com/marcysutton/js-a11y-workshop
- http://bit.ly/microsoft-inclusive-toolkit
- https://kgotgit.medium.com/accessible-angular-web-component-to-toggle-options-between-two-listbox-s-550c158d1074 full multiselect component written in Angular, awesome example
  - https://kgotgit.github.io/ng-web-accessibility/transferlist awesome components in angular, really working and complete
- https://kgotgit.medium.com/web-accessibility-autocomplete-combobox-with-manual-selection-angular-component-part-2-2f7bc1388b59 full combobox autocomplete component in Angular, awesome example
  - https://kgotgit.github.io/ng-web-accessibility/combobox awesome components in angular, really working and complete
- https://morioh.com/p/0993e06398a1 : also nice example, but not complete (missing `aria-activedescendant`)
- https://github.com/alphagov/accessible-autocomplete : awesome autocomplete component, try to rewrite it to angular

### Examples

- https://medium.com/allenhwkim/angular-build-a-dropdown-menu-f4c65e74f610 : `role=menubar` example
- https://dev.to/emmabostian/creating-a-custom-accessible-drop-down-3gmo : nice example od dropdown list, but check comments that suggest other features

## Location component tips

- check out slide 53 from accessibility course and get inspired
- check out dropdown component from repository - it has aria-controls, aria-haspopup
- multiselect: perfect for location dropdown: https://kgotgit.medium.com/accessible-angular-web-component-to-toggle-options-between-two-listbox-s-550c158d1074
  - https://kgotgit.github.io/ng-web-accessibility/transferlist - full implementation of multiselect
  - https://kgotgit.github.io/ng-web-accessibility/combobox - full combobox implementation

## Shortcuts

- Option + Cmd + I : toggle chrome dev tools
- Option + Cmd + Left / Right arrows : switch between tabs

## Todo

- read about accessible menu button
